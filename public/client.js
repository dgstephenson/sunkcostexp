var loginForm = document.getElementById("loginForm")
var startupDiv = document.getElementById("startupDiv")
var socket = io()

var id
var treatment = 1
var state = "startup"
var action = 0
var payoff = 0
var timeStep = 0.1

socket.on('updateSubject', function (msg) {
  treatment = msg.treatment
  state = msg.state
  payoff = msg.payoff
  updateinterface() 
})

updateinterface = function() {
  loginForm.style.display = "none"
  startupDiv.style.display = "block"
}

joinGame = function() {
  console.log("joinGame")
  id = parseInt(loginForm["subjectID"].value)
  if(id>0) {
    setInterval(updateServer,timeStep*1000)
  }
  return false
}

updateServer = function() {
  var msg = { 
    id: id, 
    action: action
  }
  socket.emit('clientUpdateServer',msg)
}

document.onmousedown = function(event) {
  console.log(`action = ${action}`)
  console.log(`state = ${state}`)
}

document.onmousemove = function(event) {
  action = (10*(event.x/window.innerWidth)).toFixed(0)
}