var loginForm = document.getElementById("loginForm")
var subjectsTable = document.getElementById("startupDiv")
var socket = io()

var timeStep = 0.1
var numSubjects = 0
var treatment = 1
var state = "startup"

socket.on('updateManager', function (msg) {
  numSubjects = msg.numSubjects
  state = msg.state
  updateInterface()
})

updateInterface = function() {
  loginForm.style.display = "none"
  startupDiv.style.display = "block"
}

connectToServer = function() {
  console.log("connectToServer")
  setInterval( managerUpdateServer, timeStep*1000);
  return false;
}

managerUpdateServer = function() {
  var msg = {
    treatment: treatment
  }
  socket.emit('managerUpdateServer',msg)
}

document.onmousedown = function(event) {
  console.log(`numSubjects = ${numSubjects}`)
  console.log(`state = ${state}`)
}