var express = require('express')
var app = express()
var http = require('http').Server(app)
var io = require('socket.io')(http)
var fs = require('fs')

var subjects = []
var numSubjects = 0
var treatment = 1
var state = 'startup'

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
   res.sendFile(__dirname + '/public/client.html');
})

app.get('/manager', function (req, res) {
   res.sendFile(__dirname + '/public/manager.html');
})

io.on('connection', function(socket){
  socket.on('clientUpdateServer', function(msg){
    if(typeof(subjects[msg.id]) === "undefined") {
      createSubject(msg.id);
    } else {
      subjects[msg.id].action = msg.action
      subjects[msg.id].payoff = 10-Math.abs(msg.action-5)
      updateSubject(msg.id,socket)
    }
  })
  socket.on('managerUpdateServer', function(msg){
    updateManager(socket);
  })
})

http.listen(3000, function(){
  var port = http.address().port
  console.log('listening on *:%s', port);
})

createSubject = function(id) {
  numSubjects += 1
  subjects[id] = {
    id: id,
    action: 0,
    payoff: 0
  };
  console.log(id+' connected');
}

updateSubject = function(id,socket) {
  var subject = subjects[id];
  var msg = {
    treatment: treatment,
    state: state,
    payoff: subject.payoff
  }
  socket.emit("updateSubject",msg);
}

getSubjectIds = function() {
  var ids = subjects.map(subject => subject.id).sort((a,b)=>a-b);
  ids.pop();
  return ids;
}

updateManager = function(socket) {
  const msg = {
    numSubjects: numSubjects,
    state: state
  };
  socket.emit("updateManager",msg);
}